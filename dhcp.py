#! /usr/bin/python3

import argparse
import json
import logging
import os
import sys
import ipaddress
import subprocess

import ldap
import jinja2

path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger('dhcp')
handler = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate list of allowed hosts",
    )
    parser.add_argument("-l", "--ldap-server", help="URL de la base ldap à contacter", type=str, default=None)
    parser.add_argument("-e", "--export", help="Exporte la configuration sur stdout", action="store_true")
    parser.add_argument("-q", "--quiet", help="Diminue la verbosité des logs", action='count', default=0)
    parser.add_argument("-d", "--do-not-regen", help="Do not regen the mail server", action='store_true')
    parser.add_argument("-v", "--verbose", help="Augmente la verbosité des logs", action='count', default=0)
    args = parser.parse_args()

    verbosity = args.verbose - args.quiet
    if verbosity <= -1:
        logger.setLevel(logging.WARNING)
    elif verbosity == 0:
        logger.setLevel(logging.INFO)
    elif verbosity >= 1:
        logger.setLevel(logging.DEBUG)

    logger.info("Reading configuration")
    with open(os.path.join(path, "dhcp.json")) as config_file:
        config = json.load(config_file)
    logger.debug("Config: {}".format(config))

    if args.ldap_server is not None:
        config['ldap_url'] = args.ldap_server

    logger.info("Connecting to LDAP")
    base = ldap.initialize(config['ldap_url'])
    # \begin{je_comprends_rien}
    if config['ldap_url'].startswith('ldaps://'):
        # On ne vérifie pas le certificat pour le LDAPS
        logger.debug("Using LDAPS: changing TLS context")
        base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
    # \end{je_comprends_rien}

    logger.info("Querying LDAP")
    hosts_query_id = base.search("ou=hosts,dc=crans,dc=org", ldap.SCOPE_SUBTREE, "objectClass=ipHost")
    hosts_query = base.result(hosts_query_id)[1]

    extensions = [ '.' + ext for ext in config['extensions'] ]
    machines_pool = { extension : [] for extension in extensions }

    #print(hosts_query)
    for name,machine in hosts_query:
        if type(machine) != dict:
            logging.warn("'{}' is not a valid machine descriptor for '{}'".format(machine, name))
            continue
        if 'cn' not in machine:
            logging.warn("Machine '{}' is missing 'cn'".format(name))
            continue
        cns = [ cn.decode('utf-8') for cn in machine['cn']
                if any(cn.decode('utf-8').endswith(ext) for ext in extensions) ]
        if len(cns) == 0:
            logging.info("Machine '{}' has no selected extension".format(name))
            continue
        if 'ipHostNumber' not in machine:
            logging.warn("Machine '{}' is missing 'ipHostNumber'".format(name))
            continue
        ips = [ ip.decode('utf-8') for ip in machine['ipHostNumber']
                if ipaddress.ip_address(ip.decode('utf-8')).version == 4 ]
        if len(ips) == 0:
            logging.info("Machine '{}' has no ipv4 address".format(name))
            continue
        if len(ips) > 1:
            logging.warn("Flushing excess addresses from '{}'".format(name))
        ip = ips[0]
        if 'macAddress' not in machine:
            logging.warn("Machine '{}' is missing 'macAddress'".format(name))
            continue
        macs = machine['macAddress']
        if len(macs) == 0:
            logging.warn("Machine '{}' has no mac address".format(name))
            continue
        if len(macs) > 1:
            logging.warn("Flushing excess mac addresses from '{}'".format(name))
        mac = macs[0]
        for cn in cns:
            ext = next(ext for ext in extensions if cn.endswith(ext))
            machines_pool[ext].append({
                'hostname': cn,
                'mac': mac.decode('utf-8'),
                'ip': ip,
            })

    with open(os.path.join(path, 'templates', 'list.j2')) as list_template:
        template = jinja2.Template(list_template.read())
    for extension, machines in machines_pool.items():
        if args.export:
            print('#'*(23+len(extension)))
            print(f'########## _{extension} ##########')
            print('#'*(23+len(extension)))
            print(template.render(machines=machines, extension=extension))
        else:
            with open(os.path.join(path, 'generated', f"dhcp{extension}.list"), 'w') as generated:
                generated.write(template.render(machines=machines, extension=extension))
            if not args.do_not_regen:
                subprocess.run(['/usr/bin/systemctl', 'try-restart', 'isc-dhcp-server'])

